package main

import (
	"fmt"
)

func remove[T any](slice []T, i int) []T {

	copy(slice[i:], slice[i+1:])

	sliceLen, sliceCap := len(slice), cap(slice)
	// 缩容, 保持最少64个元素
	if sliceLen < sliceCap/3 && sliceCap > 128 {
		newSlice := make([]T, sliceLen, sliceCap/2)
		copy(newSlice, slice)
		slice = newSlice
	}

	return slice[:len(slice)-1]
}

func main() {
	var numbers []int
	// append 200 elements
	for i := 0; i < 200; i++ {
		numbers = append(numbers, i)
	}

	fmt.Println(numbers, len(numbers), cap(numbers))
	// remove 100 elements
	for i := 0; i < 180; i++ {
		numbers = remove(numbers, 0)
	}
	fmt.Println(numbers, len(numbers), cap(numbers))

}
